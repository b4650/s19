// Cube Root

let num = 2

const getCube = num**3;
console.log(`The cube of ${num} is ${getCube}.`)

// Address

let address = ["258", "Washington Ave NW,", "California", 90011]
const [unit, street, state, zipCode] = address
console.log(`I live at ${unit} ${street} ${state} ${zipCode}`)

// Animal

let animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
};
const{name, type, weight, measurement} = animal
console.log(`${name} was a ${type}. He wighted at ${weight} with a measurement of ${measurement}.`)

// Number

const numbers = [1, 2, 3, 4, 5]
numbers.forEach(x => console.log(x))

const sum = numbers.reduce(
	)

// Dog

class dog{
	constructor (name, age, breed){
		this.name = name,
		this.age = age,
		this.breed = breed
	}
};
const dog1 = new dog("Frankie", 5, "Miniature Dachshund")
console.log(dog1)