// exponent operator
	// pre-es6
	const firstNum = Math.pow(8,2);
	console.log(firstNum)

	//es6 update
	const secondNum = 8**2;
	console.log(secondNum)


let name = "Rie";
let message = "Hello " + name + "! Welcome to the programming field!";
console.log(message)

// ES6 - Template literals
message = `Hello ${name}! Welcome to the programming field`;
console.log (message);

// multiline
const anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8**2 with the answer of ${secondNum}
`
console.log(anotherMessage)

// computation inside the template literals
const interestRate = .1;
const principal = 1000;
console.log(`The interest on your savings is ${principal * interestRate}`)


// Array Destructuring - allows unpacking elements in arrays into distinct variables; allows naming of array elements with variable instead of using index numberts; helps wiith the code readabiliuty and coding efficiency

//

// pre es6
const fullName = ["Juan", "Dela", "Cruz"]
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}`)

// es6
const[ firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);
console.log(`Hello ${firstName} ${middleName} ${lastName}`)

// Object destructuring - allows unpacking elements in objkect into distinct variable; shortens the syntax for accessing the objects
// pre es6

let woman = {
	givenName: "Rie",
	maidenName: "Hamayasu",
	familyName: "Riparip"
};
console.log(woman.givenName)
console.log(woman.maidenName)
console.log(woman.familyName)
console.log(`Hello ${woman.givenName} ${woman.maidenName} ${woman.familyName}!`)

//es6
// the key and variable should match
const {givenName, maidenName, familyName} = woman;
console.log(woman.givenName)
console.log(woman.maidenName)
console.log(woman.familyName)
console.log(`Hello ${woman.givenName} ${woman.maidenName} ${woman.familyName}!`)


// pre-es6 (Functions)
/*
 	declaration
 	functionName
 	parameters
 	statements
 	invoke/call the function

*/

function printFullName(firstName, middleInitial, lastName){
	console.log(firstName);
	console.log(middleInitial);
	console.log(lastName);
	console.log(firstName, middleInitial, lastName)
};

printFullName("Jane","R.","Rivera");

// ES6
// Arrow Function
/*
	compact alternative syntax to traditional functions; useful for code snippets where creating functions will not be reused in any other parts of the code; Don't repeat yourself - there is no need to create a function that will not be used in the other portions of the code.

	SYNTAX:
		declaration [const] functionName = (parameters) => {
			statements
		}
		invoke/call the function
*/

const printFName=(fname,mname,lname) =>{
	console.log(fname, mname, lname);
}
printFName("Will", "D.", "Smith");

const students = ['John', 'Jane', 'Joe']

// Pre-es6
students.forEach(
	function(pick){
		console.log(pick);
	}
);

// es6
// if you have 2 or more parameters, enclose them inside a pair of parenthesis
		/*students.forEach((x) => console.log(x))*/
students.forEach(x => console.log(x))


function addNumbers(x,y){
	return x + y
};

let total = addNumbers(1,2)
console.log(total)

//ES6

const adds = (x,y) => x + y;
// in the code above actually runs as const adds = (x,y) => return x + y;
let totals = adds(4,6);
console.log(totals)


// (Arrow function) Default Function Argument Value
/*
	provides a default argument value if no paramters are specified once the function has been invoked
*/
const greet = (name = "User") => {
	return `Good morning, ${name}`
}
console.log(greet())
// once the function has a specified parameter value
console.log(`Result of specified value parameter: ${greet("John")}`)

// Class contstructor
// ES6
/*function car(name, brand, year){
	this.name = name
	this.brand = brand
	this.year = year
};

const car1 = new car("Huayra", "Pagani", 2018)
console.log(car1)*/

// Class constructer
// class keyword declares the creation of a "car" object
// constructor keyword - special method for creating/initializing and object for the "car" class
class car{
	constructor (brand, name, year){
		this.brand = brand,
		this.name = name,
		this.year = year
	}
};
const car1 = new car("Lamborghini", "Aventador SV", 2012)
console.log(car1)

const car2 = new car();
console.log(car2);

// reassign/adding properties to an object
car2.brand = "Toyota",
car2.name = "Fortuner",
car2.year = 2020
console.log(car2)

let num = 15;
(num <= 0) ? true : false;
console.log(num)